/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.datalab1;

import java.util.Arrays;

/**
 *
 * @author ASUS
 */
public class ArrayManipulation {

    public static void main(String[] args) {
        int[] numbers = {5, 8, 3, 2, 7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];
        int sum = 0;
        double max = 0;

        int[] nums1 = {1, 2, 3, 0, 0, 0};
        int m = 3;
        int[] nums2 = {2, 5, 6};
        int n = 3;

        values[0] = 1.1;
        values[1] = 2.2;
        values[2] = 3.3;
        values[3] = 4.4;

        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
        System.out.println("");
        for (int i = 0; i < names.length; i++) {
            System.out.print(names[i] + " ");
        }
        System.out.println("");
        for (int i = 0; i < values.length; i++) {
            System.out.print(values[i] + " ");
        }
        System.out.println("");
        for (int i = 0; i < values.length; i++) {
            sum = (int) (sum + values[i]);
        }
        System.out.print("Sum = " + sum);
        System.out.println("");
        for (int i = 0; i < values.length; i++) {
            if (max < values[i]) {
                max = (double) values[i];
            }
        }
        System.out.print("Max : " + max);
        System.out.println("");
        String[] reversedNames = names;

        for (int i = reversedNames.length - 1; i >= 0; i--) {
            System.out.print(reversedNames[i] + " ");
        }
        System.out.println("");
        // array min to max
        Arrays.sort(numbers);
        for (int number : numbers) {
            System.out.print(number + " ");
        }
        System.out.println("");
        //lab 1.2
        zeroUP();
        System.out.println("");
        //lab 1.3
        Uparray(nums1, m, nums2, n);
          for (int num : nums1) {
            System.out.print(num + " ");
        }
    }

    private static void zeroUP() {
        int[] arr = {1, 0, 2, 3, 0, 4, 5, 0};
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                for (int j = arr.length - 1; j > i; j--) {
                    arr[j] = arr[j - 1];
                }
                i++;
            }
        }
        for (int num : arr) {
            System.out.print(num + " ");
        }
    }

    private static void Uparray(int[] nums1, int m, int[] nums2, int n) {
        int i = m - 1; 
        int j = n - 1; 
        int k = m + n - 1; 

        
        while (i >= 0 && j >= 0) {
            nums1[k--] = (nums1[i] >= nums2[j]) ? nums1[i--] : nums2[j--];
        }

        
        while (j >= 0) {
            nums1[k--] = nums2[j--];
        }
    }
}
